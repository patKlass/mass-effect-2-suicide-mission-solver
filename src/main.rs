//This code is copyrighted by Patrick Klassen, 2016.
//Modification is permissible, so long as you inform me that you are doing so, and you recieve confirmation of that, and you acknowledge that I created the base of your further efforts.
//Mass Effect and the characters thereof belong to Bioware.

use std::io;
use std::env;
use std::process;


//Constant definitions
//character definintions
const NONE: u32 = !0_u8 as u32;
const SHEP: u32 = 0;
const TALI: u32 = 1;
const GARRUS: u32 = 2;
const GRUNT: u32 = 3;
const SAMARA: u32 = 4;
const MIRANDA: u32 = 5;
const JACOB: u32 = 6;
const JACK: u32 = 7;
const LEGION: u32 = 8;
const THANE: u32 = 9;
const MORDIN: u32 = 10;
const ZAEED: u32 = 11;
const KASUMI: u32 = 12;

//Death-location definitions
const D_ARMOR: u8 = 0;
const D_SHIELDS: u8 = 1;
const D_WEAPONS: u8 = 2;
const D_VENTS: u8 = 3;
const D_BEES: u8 = 4;
const D_FTLEAD: u8 = 5;
const D_CREWHERD: u8 = 6;
const D_REAPER: u8 = 7;

//Trinary definitions (used for skills)
const S_NONE: u8 = 0;
const S_HALF: u8 = 1;
const S_FULL: u8 = 3;

//struct definitions
#[derive(Debug, Clone)]
struct Character {
    name: u32,
    alive: bool,
    present: u8,
    leader: u8,
    tech: u8,
    biotic: u8,
    combat: u8,
    sent_back: bool,
}

impl PartialEq for Character {
    fn eq(&self, other: &Self) -> bool {
        if self.name == other.name && self.alive == other.alive && self.sent_back == other.sent_back { true }
        else { false }
    }
}

#[derive(Debug, Clone)]
struct Normandy {
    armor: bool,
    shields: bool,
    thanix: bool,
    crew: bool,
}

impl PartialEq for Normandy {
    fn eq(&self, other: &Self) -> bool {
        if self.armor == other.armor && self.shields == other.shields && self.thanix == other.shields && self.crew == other.crew { true }
        else { false }
    }
}

#[derive(Debug, Clone)]
struct Group {
    chars: Vec<Character>,
}

impl PartialEq for Group {
    fn eq(&self, other: &Self) -> bool {
        if self.chars.len() != other.chars.len() {
            return false;
        }
        for c in 1..13 {
            if self.avail(c) ^ other.avail(c) {
                return false;
            }
        }
        return true;
    }
}

impl Group {
    
    //Will kill the character of the given Name.
    fn kill(&mut self, name: u32) {
        for c in &mut self.chars {
            if c.name == name {
                c.alive = false;
                break;
            }
        }
    }

    //With rs1 and rs2 exempt as your squadmates, this function Kills [deathcount] squadmates according
    //to the patterns of The Line.
    fn kill_on_line(&mut self, rs1: u32, rs2: u32, mut deathcount: i32) {
        let mut ld_ord: Vec<u32> = vec![MORDIN, TALI, KASUMI, JACK, MIRANDA, JACOB, GARRUS, SAMARA, LEGION, THANE, ZAEED, GRUNT];
        ld_ord.retain(|&x| x != rs1 && x != rs2 && self.avail(x));
        if deathcount != 0 {
            for i in 0..ld_ord.len() {
                let o = ld_ord[i];
                if self.avail(o) && o != rs1 && o != rs2 && !self.check_loy(o) {
                    self.kill(o);
                    deathcount -= 1;
                    if deathcount == 0 {
                        break;
                    }
                }
            }
        }
        if deathcount != 0 {
            for i in 0..ld_ord.len() {
                let o = ld_ord[i];
                if self.avail(o) && o != rs1 && o != rs2 {
                    self.kill(o);
                    deathcount -= 1;
                    if deathcount == 0 {
                        break;
                    }
                }
            }
        }
        if deathcount != 0 {
            unreachable!("failure to find enough people to kill.");
        }
    }

    //With rs1 and rs2 exempt as your squadmates, this function returns [deathcount]-long Vec of
    //who would die according to the patterns of The Line.
    fn find_line_deaths(&self, rs1: u32, rs2: u32, mut deathcount: i32) -> Vec<u32> {
        let mut ko = Vec::with_capacity(13);
        let ld_ord: [u32; 12] = [MORDIN, TALI, KASUMI, JACK, MIRANDA, JACOB, GARRUS, SAMARA, LEGION, THANE, ZAEED, GRUNT];
        if deathcount != 0 {
            for i in 0..ld_ord.len() {
                let o = ld_ord[i];
                if self.avail(o) && o != rs1 && o != rs2 && !self.check_loy(o) {
                    ko.push(o);
                    deathcount -= 1;
                    if deathcount == 0 {
                        break;
                    }
                }
            }
        }
        if deathcount != 0 {
            for i in 0..ld_ord.len() {
                let o = ld_ord[i];
                if self.avail(o) && o != rs1 && o != rs2 && self.check_loy(o) {
                    ko.push(o);
                    deathcount -= 1;
                    if deathcount == 0 {
                        break;
                    }
                }
            }
        }
        if deathcount != 0 {
            unreachable!("failure to find enough people to kill.");
        }
        ko
    }

    //returns the loyalty of the named squadmate. If absent, or not even in the group at all, will
    //return false.
    fn check_loy(&self, name: u32) -> bool {
        for c in &self.chars {
            if c.name == name {
                return c.present == S_FULL;
            }
        }
        false
    }

    //will send back the named squadmate. If absent, will do nothing.
    fn send_back(&mut self, name: u32) {
        for c in &mut self.chars {
            if c.name == name && c.present != S_NONE {
                c.sent_back = true;
            }
        }
    }

    //Will return a char-list style number of everyone who both hired and alive.
    fn living(&self) -> u32 {
        let mut l: u32 = 0;
        for c in &self.chars {
            if c.alive && c.present != S_NONE && c.name != SHEP{
                l += 2_u32.pow(c.name - 1);
            }
        }
        l
    }

    //Will return whether or not the named squadmate is 'available' for being assigned to tasks.
    fn avail(&self, name: u32) -> bool {
        for c in &self.chars {
            if c.name == name {
                return c.present != S_NONE && c.alive && !c.sent_back;
            }
        }
        false
    }

    //returns the vent skill of the named squadmate;
    //0: cannot be assigned.
    //1: can be assigned, will fail.
    //2: can be assigned, will succeed if loyal and fireteam leader is capable and loyal.
    fn vent_skill(&self, name: u32) -> u8 {
        for c in &self.chars {
            if c.name == name {
                if c.tech == S_FULL {
                    if c.present != S_FULL {
                        return S_HALF;
                    }
                }
                return c.tech;
            }
        }
        S_NONE
    }

    //returns the list of all squadmates who would succeed in the vent.
    fn ret_vent_capable(&self) -> Vec<u32> {
        let mut capable = Vec::with_capacity(13);
        for c in &self.chars {
            if c.tech == S_FULL && c.present == S_FULL && self.avail(c.name) {
                if c.name != SHEP {
                    capable.push(c.name);
                }
            }
        }
        capable
    }

    //returns the biotic skill of the named squadmate;
    //0: cannot be assigned.
    //1: can be assigned, will fail.
    //2: can be assigned, will succeed if loyal.
    fn bio_skill(&self, name: u32) -> u8 {
        for c in &self.chars {
            if c.name == name {
                if c.biotic == S_FULL {
                    if c.present != S_FULL {
                        return S_HALF;
                    }
                }
                return c.biotic;
            }
        }
        S_NONE
    }

    //returns the list of all squadmates who would succeed at holding the bubble.
    fn ret_bio_capable(&self) -> Vec<u32> {
        let mut capable = Vec::with_capacity(13);
        for c in &self.chars {
            if c.biotic == S_FULL && c.present == S_FULL && self.avail(c.name) {
                if c.name != SHEP {
                    capable.push(c.name);
                }
            }
        }
        capable
    }

    //returns the list of all squadmates who could be assigned to the bubble and would fail.
    fn ret_bio_incapable(&self) -> Vec<u32> {
        let mut incapable = Vec::with_capacity(13);
        for c in &self.chars {
            if c.biotic != S_NONE && c.present != S_NONE && c.alive {
                if c.biotic == S_HALF || c.present != S_FULL {
                    if c.name != SHEP {
                        incapable.push(c.name);
                    }
                }
            }
        }
        incapable
    }

    //returns the leadership skill of the named squadmate for the vent check.
    //0: can be assigned, would fail.
    //1 or 2: can be assigned, would succeed.
    fn vent_lead_skill(&self, name: u32) -> u8 {
        for c in &self.chars {
            if c.name == name {
                if c.present != S_FULL {
                    return S_NONE;
                }
                return c.leader;
            }
        }
        S_NONE
    }
    
    //returns the leadership skill of the named squadmate for the second fireteam check.
    //0: can be assigned, would fail.
    //1 or 2: can be assigned, would succeed.
    fn lead_skill(&self, name: u32) -> u8 {
        for c in &self.chars {
            if c.name == name {
                if c.leader == S_HALF {
                    if c.present != S_FULL {
                        return S_NONE;
                    }
                }
                return c.leader;
            }
        }
        S_NONE
    }

    //returns the list of all squadmates would would succeed for...
    //if round2 is false: the vent check
    //if round2 is true: the second fireteam check.
    fn ret_lead_capable(&self, round2: bool) -> Vec<u32> {
        let mut capable = Vec::with_capacity(13);
        for c in &self.chars {
            if c.name != SHEP && self.avail(c.name) {
                if !round2 {
                    if c.leader != S_NONE && c.present == S_FULL {
                        capable.push(c.name);
                    }
                }
                else {
                    if c.leader == S_FULL {
                        capable.push(c.name);
                    }
                    else if c.leader == S_HALF && c.present == S_FULL {
                        capable.push(c.name);
                    }
                }
            }
        }
        capable
    }

    //Returns the list of all squadmates who would fail the check for the first fireteam.
    //Tailored for Vent only!
    fn ret_lead_incapable(&self, specialist: u32) -> Vec<u32> {
        let mut incapable = Vec::with_capacity(13);
        for c in &self.chars {
            if self.avail(c.name) && c.name != specialist{
                if c.leader == S_NONE || c.present != S_FULL {
                    if c.name != SHEP {
                        incapable.push(c.name);
                    }
                }
            }
        }
        incapable
    }

    //returns the combat-skill average and the number of people for the line, with sqm1 and sqm2
    //counted as assigned to the reaperfight.
    fn line_strength(&self, sqm1: u32, sqm2: u32) -> (f32, i32) {
        let mut strength = 0;
        let mut number: i32 = 0;
        for c in &self.chars {
            if self.avail(c.name) && c.name != sqm1 && c.name != sqm2 && c.name != SHEP {
                number += 1;
                strength += c.combat;
                if self.check_loy(c.name) {
                    strength += 1;
                }
            }
        }
        let average = strength as f32 / number as f32;
        (average, number)
    }

    //returns the ordered list of who can die at the given [loc]ation. Includes NONE!
    fn get_set(&self, loc: u8) -> Vec<u32>{
        let loc_lists = vec![vec![JACK, NONE],                              // armor
            vec![KASUMI, LEGION, TALI, THANE, GARRUS, ZAEED, GRUNT, SAMARA, NONE],  // shields
            vec![THANE, GARRUS, ZAEED, GRUNT, JACK, SAMARA, NONE],          //weapons
            vec![TALI, GARRUS, JACOB, LEGION, THANE, MORDIN, KASUMI, NONE], //vents
            vec![THANE, JACK, GARRUS, LEGION, GRUNT, SAMARA, JACOB, MORDIN, TALI, KASUMI, ZAEED, NONE], //bees
            vec![TALI, GARRUS, GRUNT, SAMARA, JACOB, JACK, LEGION, THANE, MORDIN, ZAEED, KASUMI, NONE], //ftlead
            vec![TALI, GARRUS, GRUNT, SAMARA, MIRANDA, JACOB, JACK, LEGION, THANE, MORDIN, ZAEED, KASUMI, NONE],  //crewherd
            vec![TALI, GARRUS, GRUNT, SAMARA, MIRANDA, JACOB, JACK, LEGION, THANE, MORDIN, ZAEED, KASUMI, NONE],  //reaper1
            vec![TALI, GARRUS, GRUNT, SAMARA, MIRANDA, JACOB, JACK, LEGION, THANE, MORDIN, ZAEED, KASUMI, NONE]]; //reaper2
        let mut loc_list = loc_lists[loc as usize].clone();
        loc_list.retain(|&x| x == NONE || self.avail(x));
        loc_list
    }

    //returns the list of characters who pass the avail() check
    fn get_avail(&self) -> Vec<u32> {
        let mut c = self.chars.clone();
        c.retain(|sqdmate| self.avail(sqdmate.name) && sqdmate.name != SHEP);
        let mut a = Vec::with_capacity(13);
        for i in 0..c.len() {
            a.push(c[i].name);
        }
        a
    }

    //returns the list of characters who pass the avail() check and are not on the given list.
    //Tailored for the crewherder assignment, and thus adds NONE to the end of the list!
    fn get_avail_to_live(&self, to_die: &Vec<u32>) -> Vec<u32> {
        let mut c = self.chars.clone();
        c.retain(|sqdmate| self.avail(sqdmate.name) && sqdmate.name != SHEP);
        let mut a = Vec::with_capacity(13);
        for i in 0..c.len() {
            a.push(c[i].name);
        }
        a.retain(|x| !to_die.contains(x));
        a.push(NONE);
        a
    }

    //returns the list of characters who pass the avail() check and are loyal.
    fn get_avail_and_loyal(&self) -> Vec<u32> {
        let mut loy = Vec::with_capacity(12);
        for c in &self.chars {
            if self.avail(c.name) && c.name != SHEP {
                if self.check_loy(c.name) {
                    loy.push(c.name);
                }
            }
        }
        loy
    }
}

#[derive(Debug, Copy, Clone)]
struct Solution {
    oc_sqd1: u32,
    oc_sqd2: u32,
    vent: u32,
    ft_lead1: u32,
    ft_lead2: u32,
    be_sqd1: u32,
    be_sqd2: u32,
    biotic: u32,
    crewherd: u32,
    re_sqd1: u32,
    re_sqd2: u32,
    loyalties: u32,
    presences: u32,
    desired: u32,
    nor_armor: bool,
    nor_shield: bool,
    nor_thanix: bool,
}

//Returns a solution without any assignments filled in. Normandy upgrades default to true!
fn new_sol(pres: u32, loy: u32, to_die: &Vec<u32>) -> Solution {
    let mut desired = pres;
    for c in to_die {
        desired -= 2_u32.pow(c - 1);
    }
    Solution {
        oc_sqd1: NONE,
        oc_sqd2: NONE,
        vent: NONE,
        ft_lead1: NONE,
        ft_lead2: NONE,
        be_sqd1: NONE,
        be_sqd2: NONE,
        biotic: NONE,
        crewherd: NONE,
        re_sqd1: NONE,
        re_sqd2: NONE,
        loyalties: loy,
        presences: pres,
        nor_armor: true,
        nor_shield: true,
        nor_thanix: true,
        desired: desired,
    }
}

//group/character creation functions

//returns the proper data for the named character, with the character being as present as given.
fn get_squaddie(name: u32, present: u8) -> Character {
    let mut character = Character { name : name,
                alive : true, present: present,
                leader : S_NONE, tech : S_NONE,
                biotic : S_NONE, combat : S_HALF, sent_back : false };
    match name {
        SHEP => character = Character { .. character },
        TALI => character = Character {                     tech : S_FULL,                   combat : S_NONE, .. character },
        GARRUS => character = Character {  leader : S_HALF, tech : S_HALF,                   combat : S_FULL, .. character },
        GRUNT => character = Character {                                                     combat : S_FULL, .. character },
        SAMARA => character = Character {                                   biotic : S_FULL,                  .. character },
        MIRANDA => character = Character { leader : S_FULL,                 biotic : S_HALF,                  .. character },
        JACOB => character = Character {   leader : S_HALF, tech : S_HALF,  biotic : S_HALF,                  .. character },
        JACK => character = Character {                                     biotic : S_FULL, combat : S_NONE, .. character },
        LEGION => character = Character {                   tech : S_FULL,                                    .. character },
        THANE => character = Character {                    tech : S_HALF,  biotic : S_HALF,                  .. character },
        MORDIN => character = Character {                   tech : S_HALF,                   combat : S_NONE, .. character },
        ZAEED => character = Character {                                                     combat : S_FULL, .. character },
        KASUMI => character = Character {                   tech : S_FULL,                   combat : S_NONE, .. character },
        _ => unreachable!("That character ain't on the Normandy, Commander!"),
    };
    character

}

//returns a Group with the given Hireds and Loyals
//(both arguments should be in Char-List formatting).
fn get_group(hireds: u32, loyals: u32) -> Group {
    if hireds > 0b1111_1111_1111 || loyals > 0b1111_1111_1111 {
        sysex("too many people hired/loyal!", 100);
    }
    let mut list = Vec::with_capacity(13);
    if hireds == 0b1111_1111_1111 && loyals == 0b1111_1111_1111 {
        for name in 0..13_u32 {
            list.push(get_squaddie(name, S_FULL));
        }
        return Group{ chars: list };
    }
    list.push(get_squaddie(SHEP, S_FULL));
    for x in 1..13 {
        let mut present = 0;
        if hireds & 2_u32.pow(x - 1) != 0 {
            present += 1;
        }
        if loyals & 2_u32.pow(x - 1) != 0 {
            present += 2;
        }
        if present == 2 {
            sysex("Crewmember cannot be absent and loyal.", 300);
        }
        list.push(get_squaddie(x, present));
    }
    Group{ chars: list }
}


//misc tools

//Unused, returns a small and tidy string-form of who is alive and who was not hired.
//TODO: OVERHAUL, make use Group-data.
fn give_an_account(char_list: &Vec<Character>, normandy: &Normandy, hired: u32) {
    for character in char_list {
        print_char_name(character.name);
        match character.alive {
            true => print!("  is alive"),
            _ => print!(" has perished"),
        }
        if character.name != SHEP {
            match is_present(hired, character.name) {
                false => print!(" ...but wasn't hired."),
                _ => print!(""),
            }
        }
        println!("");
    }
    print!("The Normandy's crew");
    match normandy.crew {
        true => println!(" is alive"),
        _ => println!(" has perished"),
    }
}

//utility function for printing the names of characters. Will print!
fn print_char_name(c: u32) {
    match c {
        SHEP => print!("Shep"),
        TALI => print!("Tali"),
        GARRUS => print!("Garrus"),
        GRUNT => print!("Grunt"),
        SAMARA => print!("Samara"),
        MIRANDA => print!("Miranda"),
        JACOB => print!("Jacob"),
        JACK => print!("Jack"),
        LEGION => print!("Legion"),
        THANE => print!("Thane"),
        MORDIN => print!("Mordin"),
        ZAEED => print!("Zaeed"),
        KASUMI => print!("Kasumi"),
        NONE => print!("nobody"),
        _ => unreachable!("That character ain't on the Normandy, Commander!"),
    }
}

//prints whoever is loyal and non-loyal and present and non-present.
fn print_loyal_present_list(loylist: u32, preslist: u32) {
    print!("Loyalties: {:0>12b}, or...\n", loylist);
    for i in (0..12).rev() {
        if loylist & 2_u32.pow(i) == 2_u32.pow(i) {
            print_char_name((i + 1) as u32);
            print!(", ");
        }
    }
    print!("are loyal.\n");
    for i in (0..12).rev() {
        if loylist & 2_u32.pow(i) != 2_u32.pow(i) {
            print_char_name((i + 1) as u32);
            print!(", ");
        }
    }
    print!("are non-loyal.\n\n");

    print!("Present-list: {:0>12b}, or...\n", preslist);
    for i in (0..12).rev() {
        if preslist & 2_u32.pow(i) == 2_u32.pow(i) {
            print_char_name((i + 1) as u32);
            print!(", ");
        }
    }
    print!("are present.\n\n");
}

//prints a long-form of the solution-data.
fn print_solution(sol: Solution) {
    print!("The Normandy has ");
    match sol.nor_armor {
        false => print!("no armor, "),
        true => print!("Silaris armor, "),
    };
    match sol.nor_shield {
        false => print!("no shields, and "),
        true => print!("improved shields, and "),
    };
    match sol.nor_thanix {
        false => print!("only basic weaponry.\n"),
        true => print!("a Thanix cannon.\n"),
    };
    print_loyal_present_list(sol.loyalties, sol.presences);
    print!("Take ");
    print_char_name(sol.oc_sqd1);
    print!(" and ");
    print_char_name(sol.oc_sqd2);
    print!(" to fight the oculus.\nSend ");
    print_char_name(sol.vent);
    print!(" into the vents, while ");
    print_char_name(sol.ft_lead1);
    print!(" leads the second team.\nHave ");
    print_char_name(sol.biotic);
    print!(" hold the bubble, while ");
    print_char_name(sol.be_sqd1);
    print!(" and ");
    print_char_name(sol.be_sqd2);
    print!(" accompany you, and have ");
    print_char_name(sol.ft_lead2);
    print!(" lead the second team, while ");
    print_char_name(sol.crewherd);
    print!(" guides the crew back.\nBring ");
    print_char_name(sol.re_sqd1);
    print!(" and ");
    print_char_name(sol.re_sqd2);
    print!(" to fight the reaper, leaving everyone else behind.\n\nIn other words...\n");
    print_solution_cl(sol);
}

//converts the solution-data to the command-line-output format.
fn print_solution_cl(sol: Solution) {
    println!("{}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {:0>12b}, {:0>12b}",
           match sol.nor_armor { false => 0, true => 1 }, match sol.nor_shield { false => 0, true => 1 }, match sol.nor_thanix { false => 0, true => 1 },
           sol.oc_sqd1, sol.oc_sqd2, sol.vent, sol.ft_lead1,
           sol.biotic, sol.be_sqd1, sol.be_sqd2, sol.ft_lead2, sol.crewherd,
           sol.re_sqd1, sol.re_sqd2, sol.loyalties, sol.presences);
}

//returns if the character has a "1" on the proper position on the given Char-List.
fn is_present(charlist: u32, character: u32) -> bool {
    let verify = 2_u32.pow(character - 1);
    charlist & verify == verify
}

//compares the contents of two lists. If they both contain the same things, it returns true.
//Does not look at order at all.
fn lists_are_equal<T: PartialEq>(a: &Vec<T>, b: &Vec<T>) -> bool {
    if a.len() != b.len() {
        return false;
    }
    for i in 0..a.len() {
        if !a.contains(&b[i]) || !b.contains(&a[i]) {
            return false;
        }
    }
    return true;
}

//the run-checking functions

//Applies the game-mimicing Armor check.
fn apply_armor_check(mut group: Group, normandy: &Normandy) -> Group {
    if !normandy.armor {
        group.kill(JACK);
    }
    group
}

//Applies the game-mimicing Shield and Weapons checks.
fn apply_shield_and_weapons_check(mut group: Group, normandy: &Normandy, sqm1: u32, sqm2: u32) -> Group {
    if !group.avail(sqm1) {
        sysex("First Oculus squadmate is absent/dead and cannot be used.", 601);
    }
    if !group.avail(sqm2) {
        sysex("Second Oculus squadmate is absent/dead and cannot be used.", 602);
    }

    if sqm1 == sqm2 {
        sysex("Oculus squad has multiply-assigned person.", 201);
    }

    if !normandy.shields {
        let mut shd_order: Vec<u32> = vec![KASUMI, LEGION, TALI, THANE, GARRUS, ZAEED, GRUNT, SAMARA];
        shd_order.retain(|&x| x != sqm1 && x != sqm2 && group.avail(x));
        if shd_order.len() > 0 {
            group.kill(shd_order[0]);
        }
    }
    if !normandy.thanix {
        let mut wd_order: Vec<u32> = vec![THANE, GARRUS, ZAEED, GRUNT, JACK, SAMARA];
        wd_order.retain(|&x| (x == sqm1 || x == sqm2) && group.avail(x));
        if wd_order.len() > 0 {
            group.kill(wd_order[0]);
        }
    }
    group
}

//Applies the game-mimicing Vent section check.
fn apply_vent_section_check(mut group: Group, vent_spec: u32, ft_lead: u32) -> Group {
    if !group.avail(vent_spec) {
        sysex("Vent squadmate is absent/dead and cannot be used.", 603);
    }
    if !group.avail(ft_lead) {
        sysex("First Fireteam Lead squadmate is absent/dead and cannot be used.", 604);
    }
    if vent_spec == ft_lead {
        sysex("Vent section has multiply-assigned people.", 202);
    }

    if group.vent_skill(vent_spec) == S_FULL && group.vent_lead_skill(ft_lead) != S_NONE {
        return group;
    }
    group.kill(vent_spec);
    group
}

//Applies the game-mimicing Long Walk checks.
fn apply_long_walk_check(mut group: Group, mut nor: Normandy, bio: u32, ft_lead: u32, sqm1: u32, sqm2: u32, cherd: u32) -> (Group, Normandy) {
    if !group.avail(bio) {
        sysex("Biotic specialist squadmate is absent/dead and cannot be used.", 605);
    }
    if !group.avail(ft_lead) {
        sysex("Second fireteam leader squadmate is absent/dead and cannot be used.", 606);
    }
    if !group.avail(sqm1) {
        sysex("First Long Walk squadmate is absent/dead and cannot be used.", 607);
    }
    if !group.avail(sqm2) {
        sysex("Second Long Walk squadmate is absent/dead and cannot be used.", 608);
    }
    if cherd != NONE {
        if !group.avail(cherd) {
            sysex("Chosen crewherder is absent/dead and cannot be used.", 609);
        }
        if group.living().count_ones() <= 4 {
            sysex("Crewherder assigned with too few people around", 615);
        }
    }

    let mut assigned = vec!(bio, ft_lead, sqm1, sqm2, cherd);
    assigned.sort();
    assigned.dedup();
    if assigned.len() != 5 {
        sysex("Long Walk section has multiply-assigned people.", 203);
    }

    if cherd != NONE {
        group.send_back(cherd);
        if !group.check_loy(cherd) {
            group.kill(cherd);
        }
    }
    else {
        nor.crew = false;
    }

    if group.lead_skill(ft_lead) != S_FULL {
        if group.lead_skill(ft_lead) == S_NONE || !group.check_loy(ft_lead) {
            if ( group.living().count_ones() == 5 && cherd != NONE ) || group.living().count_ones() < 5 {
                if group.bio_skill(bio) == S_FULL {
                    group.kill(ft_lead);
                }
            }
            else {
                group.kill(ft_lead);
            }
        }
    }

    if group.bio_skill(bio) == S_FULL {
        return (group, nor);
    }

    let mut sk_list: Vec<u32> = vec![THANE, JACK, GARRUS, LEGION, GRUNT, SAMARA, JACOB, MORDIN, TALI, KASUMI, ZAEED];
    sk_list.retain(|&x| (x == sqm1 || x == sqm2) && group.avail(x));
    group.kill(sk_list[0]);
    (group, nor)
}

//Applies the game-mimicing Reaperfight and Line checks.
fn apply_final_check(mut group: Group, sqm1: u32, sqm2: u32) -> Group {
    if !group.avail(sqm1) && sqm1 != NONE {
        sysex("First End squadmate is absent/dead and cannot be used.", 610);
    }
    if !group.avail(sqm2) && sqm2 != NONE {
        sysex("Second End squadmate is absent/dead and cannot be used.", 611);
    }
    
    if sqm1 == sqm2 && sqm1 != NONE {
        sysex("End mission has multiply-assigned people.", 204);
    }

    if ( sqm1 == NONE || sqm2 == NONE ) && group.living().count_ones() >= 1 {
        if sqm1 == NONE && sqm2 == NONE && group.living().count_ones() == 1 {
            sysex("Assigned no squadmates to End squad when at least one was around.", 612);
        }
        if sqm1 == NONE && sqm2 == NONE && group.living().count_ones() > 1 {
            sysex("Assigned no squadmates to End squad when at least two were around.", 613);
        }
        if group.living().count_ones() >= 2 {
            sysex("Assigned only one squadmate to End squad when at least two were around.", 614);
        }
    }

    if sqm1 != NONE {
        if !group.check_loy(sqm1) {
            group.kill(sqm1)
        }
    }
    if sqm2 != NONE {
        if !group.check_loy(sqm2) {
            group.kill(sqm2)
        }
    }

    let (the_line, lineup) = group.line_strength(sqm1, sqm2);
    let dc: i32 = match lineup {
        1 => match the_line {
            2.0 => 0,
            0.0 ... 2.0 => 1,
            _ => 0,
        },
        2 => match the_line {
            0.0 => 2,
            2.0 => 0,
            0.0 ... 2.0 => 1,
            _ => 0,
        },
        3 => match the_line {
            0.0 => 3,
            2.0 => 0,
            1.0 ... 2.0 => 1,
            0.0 ... 1.0 => 2,
            _ => 0
        },
        4 => match the_line {
            0.0 => 4,
            0.5 ... 1.0 => 2,
            2.0 => 0,
            0.0 ... 0.5 => 3,
            1.0 ... 2.0 => 1,
            _ => 0,
        },
        _ => match the_line {
            2.0 => 0,
            1.5 ... 2.0 => 1,
            0.5 ... 1.5 => 2,
            0.0 ... 0.5 => 3,
            _ => 0,
        },
    };
    group.kill_on_line(sqm1, sqm2, dc);
    let living = group.living().count_ones();
    if living < 2 {
        group.kill(SHEP);
    }
    group
}

//the solution-getting functions

//Starts off the Solver functions with the assumed loyalty pattern.
//returns a tuple of whether a solution was found and the solution. If the first item is False, do
//not pay attention to what the Solution contains. It will be meaningless. (and empty)
fn main_solve(to_die: &Vec<u32>, present: u32) -> (bool, Solution) {
    let mut survivors = present;
    for i in 0..to_die.len() {
        if !is_present(present, to_die[i]) {
            sysex("cannot kill non-hired people.", 301);
        }
        survivors -= 2_u32.pow(to_die[i] - 1);
    }

    if !is_present(survivors, TALI) && !is_present(survivors, LEGION) && !is_present(survivors, KASUMI) {
        if !to_die.contains(&TALI) && !to_die.contains(&GARRUS) && !to_die.contains(&JACOB) && !to_die.contains(&LEGION) && !to_die.contains(&THANE) && !to_die.contains(&MORDIN) && !to_die.contains(&KASUMI) {
            sysex("No vent-capables present and no vent-possibles dying.", 302);
        }
    }

    if !is_present(present, TALI) && to_die.len() == 0 {
        sysex("No-one dying while Tali is absent - must kill one at the Shields.", 305);
    }

    if !is_present(survivors, SAMARA) && !is_present(survivors, JACK) && to_die.len() == 0 {
        sysex("No-one dying and no capable biotic specialist present.", 311);
    }

    if !is_present(survivors, MIRANDA) && !is_present(survivors, JACOB) && !is_present(survivors, GARRUS) && !to_die.contains(&TALI) && !to_die.contains(&GARRUS) && !to_die.contains(&JACOB) && !to_die.contains(&LEGION) && !to_die.contains(&THANE) && !to_die.contains(&MORDIN) && !to_die.contains(&KASUMI) {
        sysex("No capable fireteam leader and no possible vent-specialist dying.", 312);
    }

    {
        let sol = new_sol(present, survivors, &to_die);
        let group = get_group(present, survivors);
        'shield_death_verify: for _ in 0..1 {
            if !group.avail(TALI) {
                let sh_d_list = group.get_set(D_SHIELDS);
                for i in 0..sh_d_list.len() {
                    if i > 3 {
                        break;
                    }
                    if to_die.contains(&sh_d_list[i]) {
                        break 'shield_death_verify;
                    }
                }
                sysex("No one who can die at shields should, and Tali is absent.", 307);
            }
        }
        let (solved, ret_sol) = solve_armor(sol, group, &to_die);
        if solved {
            return (solved, ret_sol);
        }
    }
    return (false, new_sol(present, present, &to_die));
}

//Starts off the Solver functions with the provided loyalty pattern.
//returns a tuple of whether a solution was found and the solution. If the first item is False, do
//not pay attention to what the Solution contains. It will be meaningless. (and empty)
fn main_solve_loy_specified(to_die: &Vec<u32>, present: u32, loy: u32) -> (bool, Solution) {

    if !is_present(loy, TALI) && !is_present(loy, LEGION) && !is_present(loy, KASUMI) {
        if !to_die.contains(&TALI) && !to_die.contains(&GARRUS) && !to_die.contains(&JACOB) && !to_die.contains(&LEGION) && !to_die.contains(&THANE) && !to_die.contains(&MORDIN) && !to_die.contains(&KASUMI) {
            sysex("No capable techs present and no possible techs dying.", 303);
        }
    }

    if !is_present(present, TALI) && to_die.len() == 0 {
        sysex("No-one dying while Tali is absent (cannot acquire shields for normandy).", 306);
    }

    if !is_present(loy, SAMARA) && !is_present(loy, JACK) && to_die.len() == 0 {
        sysex("No-one dying and no capable biotic specialist present.", 309);
    }

    if !is_present(loy, MIRANDA) && !is_present(loy, JACOB) && !is_present(loy, GARRUS) && !to_die.contains(&TALI) && !to_die.contains(&GARRUS) && !to_die.contains(&JACOB) && !to_die.contains(&LEGION) && !to_die.contains(&THANE) && !to_die.contains(&MORDIN) && !to_die.contains(&KASUMI) {
        sysex("No capable fireteam leader and no possible vent-specialist dying.", 310);
    }

    let sol = new_sol(present, loy, &to_die);
    let group = get_group(present, loy);
    'shield_death_verify: for _ in 0..1 {
        if !group.avail(TALI) {
            let sh_d_list = group.get_set(D_SHIELDS);
            for i in 0..sh_d_list.len() {
                if i > 3 {
                    break;
                }
                if to_die.contains(&sh_d_list[i]) {
                    break 'shield_death_verify;
                }
            }
            sysex("No one who can die at shields should, and Tali is absent.", 308);
        }
    }
    let (solved, ret_sol) = solve_armor(sol, group, &to_die);
    if solved {
        return (solved, ret_sol);
    }
    return (false, new_sol(present, present, &to_die));
}

//iterates through the (two) possibilities at the Armor check.
fn solve_armor(sol: Solution, g: Group, to_die: &Vec<u32>) -> (bool, Solution) {
    let possibles = g.get_set(D_ARMOR);
    for p in possibles {
        if !to_die.contains(&p) && p != NONE {
            continue;
        }
        if p == NONE {
            let (solved, ret_sol) = solve_shields_and_weapons(sol, g.clone(), to_die);
            if solved {
                return (solved, ret_sol);
            }
            continue;
        }
        else {
            let new_sol = Solution{ nor_armor: false, .. sol };
            let mut new_to_die = to_die.clone();
            new_to_die.retain(|&x| x != p);
            let mut new_group = g.clone();
            new_group.kill(p);
            let (solved, ret_sol) = solve_shields_and_weapons(new_sol, new_group, &new_to_die);
            if solved {
                return (solved, ret_sol);
            }
        }
    }
    return (false, sol);
}

//iterates through the possibilities at the Shield and Weapons checks.
fn solve_shields_and_weapons(sol: Solution, g: Group, to_die: &Vec<u32>) -> (bool, Solution) {
    //Try killing nobody!
    if is_present(sol.presences, TALI) {
        let alive = g.get_avail();
        if alive.len() < 2 {
            return (false, sol);
        }
        let n_sol = Solution{ oc_sqd1: alive[0], oc_sqd2: alive[1], ..sol };
        let (solved, ret_sol) = solve_vent(n_sol, g.clone(), &to_die.clone());
        if solved {
            return (solved, ret_sol);
        }
    }

    let weapons_dying = g.get_set(D_WEAPONS);
    let shields_dying = g.get_set(D_SHIELDS);

    //ensure this problem can actually be solved!
    for sh_dy_check in 0..shields_dying.len() {
        if sh_dy_check == 3 {
            if !is_present(sol.presences, TALI) { //can't kill anyone on the to_die list and can't have none die because Tali is absent.
                return (false, sol);
            }
            break;
        }
        if to_die.contains(&shields_dying[sh_dy_check]) {
            break;
        }
    }

    'sh: for s_d_i in 0..shields_dying.len() {
        if s_d_i == 2 { //to try the third option on the shieldlist, we need to do Special Things, and not iterate through the weapons list.
            if to_die.contains(&shields_dying[s_d_i]) {
                let c1 = shields_dying[0];
                let c2 = shields_dying[1];
                let mut w_d = NONE;
                for i in 0..weapons_dying.len() {
                    if weapons_dying[i] == c1 || weapons_dying[i] == c2 {
                        w_d = weapons_dying[i];
                        break;
                    }
                }
                let mut n_sol = Solution{ oc_sqd1: c1, oc_sqd2: c2, nor_shield: false, ..sol};
                let mut new_g = g.clone();
                let mut n_to_die = to_die.clone();
                n_to_die.retain(|&x| x != shields_dying[s_d_i]);
                new_g.kill(shields_dying[s_d_i]);
                let (solved1, ret_sol1) = solve_vent(n_sol.clone(), new_g.clone(), &n_to_die.clone());
                if solved1 {
                    return (solved1, ret_sol1);
                }

                if to_die.contains(&w_d) {
                    n_to_die.retain(|&x| x != w_d);
                    new_g.kill(w_d);
                    n_sol.nor_thanix = false;
                    let (solved2, ret_sol2) = solve_vent(n_sol, new_g, &n_to_die);
                    if solved2 {
                        return (solved2, ret_sol2);
                    }
                }
            }
            break;
        }

        'we: for w_d_i in 0..weapons_dying.len() {

            let sh_to_d = shields_dying[s_d_i];
            let we_to_d = weapons_dying[w_d_i]; //aka: possible oc_sqd1
            let sh_shld_d = to_die.contains(&sh_to_d) && sh_to_d != NONE;
            let we_shld_d = to_die.contains(&we_to_d) && we_to_d != NONE;
            if !sh_shld_d && !g.avail(TALI) { //not wanting to kill the shield-possibility and not having TALI, means a fail
                continue 'sh;
            }
            if !sh_shld_d && !we_shld_d { //neither are to be killed - also covers both being "NONE"
                continue 'we;
            }
            else if !we_shld_d { //only shields dying
                let mut c1 = shields_dying[1-s_d_i];
                let mut c2 = shields_dying[1+s_d_i];
                let avail = g.get_avail();
                if c1 == NONE { //c1 will only be none if s_d_i is 0 and shields_dying.len() == 2.
                    for i in 0..avail.len() {
                        if avail[i] != shields_dying[s_d_i] {
                            c1 = avail[i];
                            break;
                        }
                    }
                }
                if c2 == NONE || c2 == c1 { //c2 will only be none if s_d_i is shields_dying.len() - 1.
                    for i in 0..avail.len() {
                        if avail[i] != shields_dying[s_d_i] && avail[i] != c1 {
                            c2 = avail[i];
                            break;
                        }
                    }
                }
                let n_sol = Solution{ nor_shield: false, oc_sqd1: c1, oc_sqd2: c2, ..sol };
                let mut n_g = g.clone();
                let mut n_to_die = to_die.clone();
                n_g.kill(sh_to_d);
                n_to_die.retain(|&x| x != sh_to_d);
                let (solved, ret_sol) = solve_vent(n_sol, n_g, &n_to_die);
                if solved {
                    return (solved, ret_sol);
                }
            }
            else if !sh_shld_d { //only weapons dying
                //treat we_to_d as c1, for the duration of this indent!
                let mut c2 = weapons_dying[w_d_i + 1];
                if c2 == NONE {
                    let avail = g.get_avail();
                    for i in 0..avail.len() {
                        if !weapons_dying[..w_d_i + 1].contains(&avail[i]) {
                            c2 = avail[i];
                            break;
                        }
                    }
                }
                let n_sol = Solution{ nor_thanix: false, oc_sqd1: we_to_d, oc_sqd2: c2, ..sol};
                let mut n_g = g.clone();
                let mut n_to_die = to_die.clone();
                n_g.kill(we_to_d);
                n_to_die.retain(|&x| x != we_to_d);
                let (solved, ret_sol) = solve_vent(n_sol, n_g, &n_to_die);
                if solved {
                    return (solved, ret_sol);
                }
            }
            else { //both dying
                if sh_to_d == we_to_d {
                    continue 'we;
                }
                let c2 = shields_dying[1-s_d_i];
                if weapons_dying[..w_d_i + 1].contains(&c2) {
                    continue 'we;
                }
                let n_sol = Solution{ nor_shield: false, nor_thanix: false, oc_sqd1: we_to_d, oc_sqd2: c2, ..sol};
                let mut n_g = g.clone();
                let mut n_to_die = to_die.clone();
                n_g.kill(we_to_d);
                n_g.kill(sh_to_d);
                n_to_die.retain(|&x| x != we_to_d && x != sh_to_d);
                let (solved, ret_sol) = solve_vent(n_sol, n_g, &n_to_die);
                if solved {
                    return (solved, ret_sol);
                }
            }
        }
    }
    //abort
    return (false, sol);
}

//Iterates through the possibilities at the Vent check. (also assigns first fireteam lead).
fn solve_vent(sol: Solution, group: Group, to_die: &Vec<u32>) -> (bool, Solution) {
    let vent_poss = group.get_set(D_VENTS);
    for v in vent_poss {
        if v != NONE && !to_die.contains(&v) {
            continue;
        }
        if v == NONE {
            let spec = group.ret_vent_capable();
            if spec.len() < 1 {
                continue;
            }
            let mut leads = group.ret_lead_capable(false);
            leads.retain(|&x| x != spec[0]);
            if leads.len() < 1 {
                continue;
            }
            let new_sol = Solution{ vent: spec[0], ft_lead1: leads[0], ..sol };
            let (solved, ret_sol) = solve_fireteam(new_sol, group.clone(), &to_die);
            if solved {
                return (solved, ret_sol);
            }
            continue;
        }
        else {
            let mut new_group = group.clone();
            new_group.kill(v);
            let mut n_to_die = to_die.clone();
            n_to_die.retain(|&x| x != v);
            let mut leads = new_group.ret_lead_incapable(v);
            if new_group.vent_skill(v) != S_FULL {
                leads.append(&mut new_group.ret_lead_capable(false));
            }
            leads.retain(|&x| x != v && new_group.avail(x));
            if leads.len() < 1 {
                continue;
            }
            let new_sol = Solution{ vent: v, ft_lead1: leads[0], ..sol };
            let (solved, ret_sol) = solve_fireteam(new_sol, new_group, &n_to_die);
            if solved {
                return (solved, ret_sol);
            }
        }
    }
    return (false, sol);
}

//Iterates through the possibilities at the Second Fireteam Lead check.
fn solve_fireteam(sol: Solution, group: Group, to_die: &Vec<u32>) -> (bool, Solution) {
    let ftl_poss = group.get_set(D_FTLEAD);
    for f in ftl_poss {
        if !to_die.contains(&f) && f != NONE {
            continue;
        }
        if f == NONE {
            let leads = group.ret_lead_capable(true);
            for i in 0..leads.len() {
                let new_sol = Solution{ ft_lead2: leads[i], .. sol };
                let (solved, ret_sol) = solve_crewherd(new_sol, group.clone(), &to_die);
                if solved {
                    return (solved, ret_sol);
                }
            }
            continue;
        }
        else {
            if group.lead_skill(f) != S_NONE {
                continue;
            }
            let mut n_group = group.clone();
            n_group.kill(f);
            let mut n_to_die = to_die.clone();
            n_to_die.retain(|&x| x != f);
            let new_sol = Solution{ ft_lead2: f, .. sol };
            let (solved, ret_sol) = solve_crewherd(new_sol, n_group, &n_to_die);
            if solved {
                return (solved, ret_sol);
            }
        }
    }
    return (false, sol);
}

//iterates through the possibilities at the Crewherd check.
fn solve_crewherd(sol: Solution, group: Group, to_die: &Vec<u32>) -> (bool, Solution) {
    let crh_poss = group.get_set(D_CREWHERD);
    for h in crh_poss {
        if (!to_die.contains(&h) && h != NONE) || h == sol.ft_lead2 {
            continue;
        }
        if h == NONE {
            if group.living().count_ones() < 4 || (group.lead_skill(sol.ft_lead2) != S_NONE && group.living().count_ones() == 4 ) {
                let (solved, ret_sol) = solve_bees(sol.clone(), group.clone(), &to_die);
                if solved {
                    return (solved, ret_sol);
                }
                continue;
            }
            else {
                let surv = group.get_avail_and_loyal();
                for i in surv {
                    if i == sol.ft_lead2 {
                        continue;
                    }
                    let n_sol = Solution{crewherd: i, ..sol};
                    let mut n_group = group.clone();
                    n_group.send_back(i);
                    let (solved, ret_sol) = solve_bees(n_sol, n_group, &to_die);
                    if solved {
                        return (solved, ret_sol);
                    }
                }
            }
            let (solved, ret_sol) = solve_bees(sol.clone(), group.clone(), &to_die);
            if solved {
                return (solved, ret_sol);
            }
            continue;
        }
        else {
            if group.living().count_ones() < 4 || (group.lead_skill(sol.ft_lead2) != S_NONE && group.living().count_ones() == 4 ) {
                continue;
            }
            let mut n_group = group.clone();
            n_group.kill(h);
            let mut n_to_die = to_die.clone();
            n_to_die.retain(|&x| x != h);
            let n_sol = Solution{ crewherd: h, .. sol };
            let (solved, ret_sol) = solve_bees(n_sol, n_group, &n_to_die);
            if solved {
                return (solved, ret_sol);
            }
        }
    }
    return (false, sol);
}

//iterates through the possibilities at the Biotic specialist check.
fn solve_bees(sol: Solution, group: Group, to_die: &Vec<u32>) -> (bool, Solution) {
    let bee_poss = group.get_set(D_BEES);
    for b_p_i in 0..bee_poss.len() {
        let b = bee_poss[b_p_i];
        if (!to_die.contains(&b) && b != NONE) || b == sol.ft_lead2 || b == sol.crewherd {
            continue;
        }
        if b == NONE {
            let mut spec = group.ret_bio_capable();
            spec.retain(|&x| x != sol.ft_lead2 && x != sol.crewherd);
            if spec.len() < 1 {
                continue;
            }
            let mut avail = group.get_avail();
            avail.retain(|&x| x != sol.ft_lead2 && x != sol.crewherd && x != spec[0]);
            if avail.len() < 2 {
                continue;
            }
            let new_sol = Solution{ biotic: spec[0], be_sqd1: avail[0], be_sqd2: avail[1], .. sol };
            let (solved, ret_sol) = solve_reaper(new_sol, group.clone(), &to_die);
            if solved {
                return (solved, ret_sol);
            }
            continue;
        }
        else {
            if bee_poss.len() - b_p_i <= 2 {
                continue;
            }
            if bee_poss[b_p_i + 1] == sol.ft_lead2 || bee_poss[b_p_i + 1] == sol.crewherd {
                continue;
            }
            let mut spec = group.ret_bio_incapable();
            spec.retain(|&x| x != b && x != bee_poss[b_p_i + 1] && x != sol.ft_lead2 && x != sol.crewherd);
            if spec.len() < 1 {
                continue;
            }
            let new_sol = Solution{ biotic: spec[0], be_sqd1: b, be_sqd2: bee_poss[b_p_i + 1], .. sol };
            let mut n_group = group.clone();
            n_group.kill(b);
            let mut n_to_die = to_die.clone();
            n_to_die.retain(|&x| x != b);
            let (solved, ret_sol) = solve_reaper(new_sol, n_group, &n_to_die);
            if solved {
                return (solved, ret_sol);
            }
        }
    }
    return (false, sol);
}

//iterates through the possibilities for who to bring to the Reaperfight.
fn solve_reaper(sol: Solution, group:Group, to_die: &Vec<u32>) -> (bool, Solution) {
    let mut r_poss = group.get_set(D_REAPER);
    r_poss.append(&mut group.get_avail_to_live(&to_die));
    r_poss.sort();
    r_poss.dedup();
    r_poss.pop(); //NONE needs to be ignored for this, so pop it off.
    if r_poss.len() < 2 {
        return (false, sol); //I have no idea how you can do this with a proper setup.
    }
    else {
        for i in 0..r_poss.len()-1 {
            for j in i+1..r_poss.len() {
                let n_sol = Solution { re_sqd1: r_poss[i], re_sqd2: r_poss[j], .. sol };
                let mut n_to_die = to_die.clone();
                let mut n_group = group.clone();
                if to_die.contains(&r_poss[i]) && r_poss[i] != NONE {
                    n_to_die.retain(|&x| x != r_poss[i]);
                    n_group.kill(r_poss[i]);
                }
                if to_die.contains(&r_poss[j]) && r_poss[i] != NONE {
                    n_to_die.retain(|&x| x != r_poss[j]);
                    n_group.kill(r_poss[j]);
                }
                let (solved, ret_sol) = solve_line(n_sol, n_group, &n_to_die);
                if solved {
                    return (solved, ret_sol);
                }
            }
        }
    }
    return (false, sol);
}

//figures out who would die at the line for the given solution.
fn solve_line(sol: Solution, group: Group, to_die: &Vec<u32>) -> (bool, Solution) {
    let (avg, num) = group.line_strength(sol.re_sqd1, sol.re_sqd2);
    let dc: i32 = match num {
        1 => match avg {
            2.0 => 0,
            0.0 ... 2.0 => 1,
            _ => 0,
        },
        2 => match avg {
            0.0 => 2,
            2.0 => 0,
            0.0 ... 2.0 => 1,
            _ => 0,
        },
        3 => match avg {
            0.0 => 3,
            2.0 => 0,
            1.0 ... 2.0 => 1,
            0.0 ... 1.0 => 2,
            _ => 0
        },
        4 => match avg {
            0.0 => 4,
            0.5 ... 1.0 => 2,
            2.0 => 0,
            0.0 ... 0.5 => 3,
            1.0 ... 2.0 => 1,
            _ => 0,
        },
        _ => match avg {
            2.0 => 0,
            1.5 ... 2.0 => 1,
            0.5 ... 1.5 => 2,
            0.0 ... 0.5 => 3,
            _ => 0,
        },
    };
    let will_die = group.find_line_deaths(sol.re_sqd1, sol.re_sqd2, dc);
    if lists_are_equal(&will_die, &to_die) {
        let success = check_sol(sol);
        if success {
            return (true, sol);
        }
    }
    return (false, sol);
}

//tests the solution against the Runner functions.
fn check_sol(sol: Solution) -> bool {
    //println!("\n{:?}", sol);
    let recieved = cl_to_result(sol);
    recieved == sol.desired
}

//the user-run acquisition functions

//gets user cmd-line input for who was hired and who was loyal, Char-list style.
fn get_user_squad() -> (u32, u32) {
    println!("The following list of people is your template: Tali, Garrus, Grunt, Samara, Miranda, Jacob, Jack, Legion, Thane, Mordin, Zaeed, Kasumi.");
    println!("Did you hire them? type a 0 if not, and a 1 if so, for each in the above list.");
    let mut hireds = String::new();
    io::stdin().read_line(&mut hireds)
        .ok()
        .expect("failed to read line");
    hireds = hireds.trim().to_string();
    let mut hireds_num: u32 = 0;
    for (i, character) in hireds.chars().rev().enumerate() {
        let num = match character.to_digit(32) {
            Some(num) => num,
            None => 0,
        };
        hireds_num += num * 2_u32.pow(i as u32);
    }
    println!("Did you make them loyal? Type a 0 if not, and a 1 if so, for each in the above list.");
    let mut loyals = String::new();
    io::stdin().read_line(&mut loyals)
        .ok()
        .expect("failed to read line");
    loyals = loyals.trim().to_string();
    let mut loyals_num: u32 = 0b0;
    for (i, character) in loyals.chars().rev().enumerate() {
        let num = match character.to_digit(32) {
            Some(num) => num,
            None => 0,
        };
        loyals_num += num * 2_u32.pow(i as u32);
    }
    return (hireds_num, loyals_num);
}

//gets user cmd-line input for normandy upgrade status.
fn get_user_normandy() -> Normandy {
    let mut normandy = Normandy { armor : false, shields : false, thanix : false, crew : true };
    println!("Does your normandy have upgraded armor? (1 = yes, 0 = no)");
    let armor = get_user_assignment();
    if armor != 0 {
        normandy.armor = true;
    }
    println!("Does your normandy have upgraded shields? (1 = yes, 0 = no)");
    let shields = get_user_assignment();
    if shields != 0 {
        normandy.shields = true;
    }
    println!("Does your normandy have a thanix cannon? (1 = yes, 0 = no)");
    let thanix = get_user_assignment();
    if thanix != 0 {
        normandy.thanix = true;
    }
    return normandy;
}

//gets user cmd-line input for oculus squad.
fn get_user_shield_and_weapons_solution(group: &Group) -> (u32, u32) {
    println!("Choose a squadmate to fight the oculus with. type in the place in which they appear here: Tali, Garrus, Grunt, Samara, Miranda, Jacob, Jack, Legion, Thane, Mordin, Zaeed, Kasumi.");
    let mut sqd1: u32;
    loop {
        sqd1 = get_user_assignment();
        if !group.avail(sqd1) {
            println!("that person isn't disposed to aid you right now");
            continue;
        }
        break;
    }
    println!("Choose a second squadmate to fight the oculus with. (same procedure)");
    let mut sqd2: u32;
    loop {
        sqd2 = get_user_assignment();
        if !group.avail(sqd2) {
            println!("that person isn't disposed to aid you right now");
            continue;
        }
        if sqd2 == sqd1 {
            println!("you can't bring the same person twice!");
            continue;
        }
        break;
    }
    (sqd1, sqd2)
}

//gets user cmd-line input for vent check assignments.
fn get_user_vent_solution(group: &Group) -> (u32, u32) {
    println!("Choose a squadmate to go through the vents. type in the place in which they appear here: Tali, Garrus, Grunt, Samara, Miranda, Jacob, Jack, Legion, Thane, Mordin, Zaeed, Kasumi.");
    let mut vent: u32;
    loop {
        vent = get_user_assignment();
        if !group.avail(vent) {
            println!("that person isn't disposed to aid you right now");
            continue;
        }
        break;
    }
    println!("Choose a squadmate to lead the second team. (same procedure)");
    let mut fireteam: u32;
    loop {
        fireteam = get_user_assignment();
        if !group.avail(fireteam) {
            println!("that person isn't disposed to aid you right now");
            continue;
        }
        if fireteam == vent {
            println!("you can't bring the same person twice!");
            continue;
        }
        break;
    }
    (vent, fireteam)
}

//gets user cmd-line input for long-walk assignments.
fn get_user_long_walk_solution(group: &Group) -> (u32, u32, u32, u32, u32) {
    println!("Choose a squadmate to hold the bubble. type in the place in which they appear here: Tali, Garrus, Grunt, Samara, Miranda, Jacob, Jack, Legion, Thane, Mordin, Zaeed, Kasumi.");
    let mut bubble: u32;
    loop {
        bubble = get_user_assignment();
        if !group.avail(bubble) {
            println!("that person isn't disposed to aid you right now");
            continue;
        }
        break;
    }
    println!("Choose a squadmate to lead the second team. (same procedure)");
    let mut fireteam: u32;
    loop {
        fireteam = get_user_assignment();
        if !group.avail(fireteam) {
            println!("that person isn't disposed to aid you right now");
            continue;
        }
        if fireteam == bubble {
            println!("You can't bring the same person twice!");
            continue;
        }
        break;
    }
    println!("Choose a squadmate to come with you. (same procedure)");
    let mut sqd1: u32;
    loop {
        sqd1 = get_user_assignment();
        if !group.avail(sqd1) {
            println!("that person isn't disposed to aid you right now");
            continue;
        }
        if sqd1 == bubble || sqd1 == fireteam {
            println!("You can't bring the same person twice!");
            continue;
        }
        break;
    }
    println!("Choose a second squadmate to come with you. (same procedure)");
    let mut sqd2: u32;
    loop {
        sqd2 = get_user_assignment();
        if !group.avail(sqd2) {
            println!("that person isn't disposed to aid you right now");
            continue;
        }
        if sqd2 == bubble || sqd2 == fireteam || sqd2 == sqd1 {
            println!("You can't bring the same person twice!");
            continue;
        }
        break;
    }
    println!("Choose someone to go back with the crew. type in the place in which they appear here: Tali, Garrus, Grunt, Samara, Miranda, Jacob, Jack, Legion, Thane, Mordin, Zaeed, Kasumi. If you want nobody to return with them, input 32");
    let mut crew_escort: u32;
    loop {
        crew_escort = get_user_assignment();
        if crew_escort == 32 {
            crew_escort = NONE;
            break;
        }
        if !group.avail(crew_escort) {
            println!("that person isn't disposed to aid you right now");
            continue;
        }
        if crew_escort == bubble || crew_escort == fireteam || crew_escort == sqd1 || crew_escort == sqd2 {
            println!("You can't bring the same person twice!");
            continue;
        }
        break;
    }
    (bubble, fireteam, sqd1, sqd2, crew_escort)
}

//gets user cmd-line input for reaper-fight squad (and hence, The Line composition)
fn get_user_line_solution(group: &Group) -> (u32, u32) {
    println!("Choose a squadmate to fight the reaperlarva with. type in the place in which they appear here: Tali, Garrus, Grunt, Samara, Miranda, Jacob, Jack, Legion, Thane, Mordin, Zaeed, Kasumi.");
    let mut sqd1: u32;
    loop {
        sqd1 = get_user_assignment();
        if !group.avail(sqd1) {
            println!("that person isn't disposed to aid you right now");
            continue;
        }
        break;
    }
    println!("Choose a second squadmate to fight the reaperlarva with. (same procedure)");
    let mut sqd2: u32;
    loop {
        sqd2 = get_user_assignment();
        if !group.avail(sqd2) {
            println!("that person isn't disposed to aid you right now");
            continue;
        }
        if sqd2 == sqd1 {
            println!("you can't bring the same person twice!");
            continue;
        }
        break;
    }
    (sqd1, sqd2)
}

//the groundwork function for actually getting any one of the above assignments.
fn get_user_assignment() -> u32 {
    let mut char_choice = String::new();
    io::stdin().read_line(&mut char_choice)
        .ok()
        .expect("failed to read line");
    let char_choice: u32 = match char_choice.trim().parse() {
        Ok(num) => num,
        Err(_) => sysex("not a useable number", 101) as u32,
    };
    char_choice
}


//the user's desired acquisition functions

//gets the desired deathlist from the user, via cmd-line input.
fn get_user_dead_list() -> Vec<u32> {
    let mut deadlist: Vec<u32> = Vec::new();
    loop {
        println!("Enter another character that you want dead (no repeats. SERIOUSLY).\n 0 - done, 1... Tali, Garrus, Grunt, Samara, Miranda, Jacob, Jack, Legion, Thane, Mordin, Zaeed, Kasumi.");
        let next_char = get_user_assignment();
        if next_char == 0 {
            return deadlist;
        }
        deadlist.push(next_char);
    }
}

//gets the desired absentlist from the user, via cmd-line input.
fn get_user_present_list() -> u32 {
    let mut present = 0b1111_1111_1111_u32;
    loop {
        println!("Enter a character you do not have, in the following order:\n  0 - Done, 1... Tali, Garrus, Grunt, Samara, Miranda, Jacob, Jack, Legion, Thane, Mordin, Zaeed, Kasumi.");
        let input = get_user_assignment();
        if input == 0 {
            break;
        }
        else if !is_present(present, input) {
            println!("Already not present!");
            continue;
        }
        else {
            present -= 2_u32.pow(input - 1);
        }
    }
    present
}


//the cmdline-to-useable conversion functions

//returns a list of everyone who is on the Char-List style Dead argument.
fn cl_to_deathgroup(dead: u32, present: u32) -> Vec<u32> {
    let mut deadlist = Vec::with_capacity(12);
    for i in 1..13 {
        if !is_present(dead, i) {
            if !is_present(present, i) {
                sysex("People cannot be both not hired and killed.", 304);
            }
            deadlist.push(i);
        }
    }
    deadlist
}

//returns the Solution specified by the cmd-line input list.
fn cl_to_solution(a: Vec<u32>) -> Solution {
    Solution {
        nor_armor: a[0] == 1, 
        nor_shield: a[1] == 1, 
        nor_thanix: a[2] == 1, 
        oc_sqd1: a[3], 
        oc_sqd2: a[4],
        vent: a[5],
        ft_lead1: a[6],
        biotic: a[7],
        be_sqd1: a[8],
        be_sqd2: a[9], 
        ft_lead2: a[10],
        crewherd: a[11],
        re_sqd1: a[12],
        re_sqd2: a[13],
        loyalties: a[14],
        presences: a[15],
        desired: a[14] 
    }
}

//returns who survives (in Char-List format) from the specified solution.
//Absentees are not differentiated from the dead!
fn cl_to_result(sol: Solution) -> u32 {
    let nor = Normandy{
        armor : sol.nor_armor, 
        shields : sol.nor_shield,
        thanix : sol.nor_thanix,
        crew : true };
    let mut charlist = get_group(sol.presences, sol.loyalties);
    charlist = apply_armor_check(charlist, &nor);
    charlist = apply_shield_and_weapons_check(charlist, &nor, sol.oc_sqd1, sol.oc_sqd2);
    charlist = apply_vent_section_check(charlist, sol.vent, sol.ft_lead1);
    let (mut charlist, _) = apply_long_walk_check(charlist, nor, sol.biotic, sol.ft_lead2, sol.be_sqd1, sol.be_sqd2, sol.crewherd);
    charlist = apply_final_check(charlist, sol.re_sqd1, sol.re_sqd2);
    charlist.living()
}

//um. the main function. Will handle command-line calling and the user-responsive calling styles.
fn main() {
    if env::args().count() > 1 {
        if env::args().nth(1).unwrap() == "solve" && env::args().count() == 4 {
            let dead = env::args().nth(2).unwrap();
            let dead: u32 = match u32::from_str_radix(dead.trim(), 2) {
                Ok(num) => num,
                Err(_) => sysex("non-binary death-list given.", 103) as u32,
            };
            let present = env::args().nth(3).unwrap();
            let present: u32 = match u32::from_str_radix(present.trim(), 2) {
                Ok(num) => num,
                Err(_) => sysex("non-binary hired-list given", 104) as u32,
            };
            let deadlist = cl_to_deathgroup(dead, present);
            //println!("{:0>12b}, {:0>12b}\n{:?}", dead, present, deadlist);
            let (solved, solution) = main_solve(&deadlist, present);
            if solved {
                print_solution_cl(solution);
            }
            else {
                sysex("No solution found", 404);
            }
        }
        if env::args().nth(1).unwrap() == "solve" && env::args().count() == 5 {
            let dead = env::args().nth(2).unwrap();
            let dead: u32 = match u32::from_str_radix(dead.trim(), 2) {
                Ok(num) => num,
                Err(_) => sysex("non-binary death-list given.", 103) as u32,
            };
            let present = env::args().nth(3).unwrap();
            let present: u32 = match u32::from_str_radix(present.trim(), 2) {
                Ok(num) => num,
                Err(_) => sysex("non-binary hired-list given", 104) as u32,
            };
            let loyal = env::args().nth(4).unwrap();
            let loyal: u32 = match u32::from_str_radix(loyal.trim(), 2) {
                Ok(num) => num,
                Err(_) => sysex("non-binary loyal-list given", 106) as u32,
            };
            let deadlist = cl_to_deathgroup(dead, present);
            //println!("{:0>12b}, {:0>12b}\n{:?}", dead, present, deadlist);
            let (solved, solution) = main_solve_loy_specified(&deadlist, present, loyal);
            if solved {
                print_solution_cl(solution);
            }
            else {
                sysex("No solution found", 404);
            }
        }
        if env::args().nth(1).unwrap() == "result" && env::args().count() == 18 {
            let mut solution = Vec::new();
            for i in 2..16 {
                solution.push(match u32::from_str_radix(&env::args().nth(i).unwrap(), 10) {
                    Ok(num) => num,
                    Err(_) => sysex("non-number assignment given", 105) as u32,
                });
            }
            for i in 16..18 {
                solution.push(u32::from_str_radix(&env::args().nth(i).unwrap(), 2).unwrap());
            }
            let solution = cl_to_solution(solution);
            let result = cl_to_result(solution);
            println!("{:0>12b}", result);
        }
    }
    else {
        loop {
            println!("Do you want to see if your solution works (0), or get a guaranteed solution (1), OR, get a solution with a specified set of loyalties (2)?");
            let solution_type = get_user_assignment();
            if solution_type == 0 {
                let (hireds, loyals) = get_user_squad();
                let mut full_char_list = get_group(hireds, loyals);
                let normandy = get_user_normandy();
                full_char_list = apply_armor_check(full_char_list, &normandy );
                let (sqd1, sqd2) = get_user_shield_and_weapons_solution(&full_char_list);
                full_char_list = apply_shield_and_weapons_check(full_char_list, &normandy, sqd1, sqd2);
                let (vent, fireteam1) = get_user_vent_solution(&full_char_list);
                full_char_list = apply_vent_section_check(full_char_list, vent, fireteam1);
                let (bubble, fireteam2, sqd1, sqd2, crew_escort) = get_user_long_walk_solution(&full_char_list);
                let (mut full_char_list, normandy) = apply_long_walk_check(full_char_list, normandy, bubble, fireteam2, sqd1, sqd2, crew_escort);
                let (sqd1, sqd2) = get_user_line_solution(&full_char_list);
                full_char_list = apply_final_check(full_char_list, sqd1, sqd2);
                give_an_account(&full_char_list.chars, &normandy, hireds);
            }
            else if solution_type == 1 {
                let dead = get_user_dead_list();
                let present = get_user_present_list();
                let (solved, solution) = main_solve(&dead, present);
                println!("");
                if !solved {
                    println!("there was no solution found");
                }
                else {
                    print_solution(solution);
                }
            }
            else if solution_type == 2 {
                let dead = get_user_dead_list();
                let (present, loyal) = get_user_squad();
                let (solved, solution) = main_solve_loy_specified(&dead, present, loyal);
                println!("");
                if !solved {
                    println!("there was no solution found");
                }
                else {
                    print_solution(solution);
                }
            }
            else {
                break;
            }
        }
    }
}

//crashes the program, with the designated error code (+147000) and will print the given String beforehand.
fn sysex(s: &str, err: i32) -> i32 {
    println!("Err: {}; {}", 147000 + err, s);
    process::exit(err);
    //err
}

//tests
#[test]
fn check_armor_check() {
    let mut group = get_group(4095, 4095);
    let normandy = Normandy { armor: false, shields: false, thanix: false, crew: true };
    let group2 = apply_armor_check(group.clone(), &normandy);
    group.kill(JACK);
    assert_eq!(group, group2);
}

#[test]
fn check_shield_and_weapons_check() {
    let mut group = get_group(4095, 4095);
    let normandy = Normandy { armor: false, shields: false, thanix: false, crew: true };
    let group2 = apply_shield_and_weapons_check(group.clone(), &normandy, MIRANDA, JACOB);
    group.kill(KASUMI);
    assert_eq!(group, group2);
    let group3 = apply_shield_and_weapons_check(group.clone(), &normandy, THANE, JACOB);
    group.kill(LEGION);
    group.kill(THANE);
    assert_eq!(group, group3);
}

#[test]
fn check_vent_check() {
    let mut group = get_group(4095, 4095);
    let group2 = apply_vent_section_check(group.clone(), 1, 2);
    assert_eq!(group2, group);
    let group3 = apply_vent_section_check(group.clone(), 1, 3);
    let mut group3_correct = group.clone();
    group3_correct.kill(TALI);
    assert_eq!(group3_correct, group3);
    let group4 = apply_vent_section_check(group.clone(), 6, 2);
    group.kill(JACOB);
    assert_eq!(group, group4);
    let group5 = apply_vent_section_check(group.clone(), 12, 3);
    group.kill(KASUMI);
    assert_eq!(group, group5);
}

#[test]
fn check_long_walk_check() {
    let nor = Normandy{armor:true, shields:true, thanix:true, crew: true};
    let mut group = get_group(4095, 4095);
    let (group2, _) = apply_long_walk_check(group.clone(), nor.clone(), SAMARA, GARRUS, THANE, MIRANDA, NONE);
    assert!(group == group2);
    let (group3, _) = apply_long_walk_check(group.clone(), nor.clone(), SAMARA, GRUNT, THANE, MIRANDA, NONE);
    group.kill(GRUNT);
    assert!(group == group3);
    let (group4, _) = apply_long_walk_check(group.clone(), nor.clone(), MIRANDA, GARRUS, THANE, JACOB, NONE);
    group.kill(THANE);
    assert!(group == group4);
    let (group5, _) = apply_long_walk_check(group.clone(), nor.clone(), MIRANDA, SAMARA, JACK, JACOB, NONE);
    group.kill(SAMARA);
    group.kill(JACK);
    assert!(group == group5);
}

#[test]
fn check_final_check() {
    let group = get_group(4095, 4095);
    let group2 = apply_final_check(group.clone(), MIRANDA, JACOB);
    assert_eq!(group, group2);
    println!("first test success");
    let group3 = get_group(4095, 0);
    let mut group4 = group3.clone();
    group4.kill(TALI);
    group4.kill(MORDIN);
    group4.kill(MIRANDA);
    group4.kill(JACOB);
    let group3 = apply_final_check(group3, MIRANDA, JACOB);
    assert_eq!(group3, group4);
    println!("second test success");
    let mut group5 = get_group(4095, 2_u32.pow(MORDIN - 1));
    let mut group6 = group5.clone();
    group6.kill(TALI);
    group6.kill(KASUMI);
    group6.kill(GARRUS);
    group6.kill(JACOB);
    group5 = apply_final_check(group5, GARRUS, JACOB);
    assert_eq!(group5, group6);
}

#[test]
fn check_niche_final() {
    let mut group = get_group(4095, 0b111101000001);
    group.kill(TALI);
    group.kill(GARRUS);
    group.kill(GRUNT);
    group.kill(SAMARA);
    group.kill(JACK);
    group.kill(LEGION);

    group.kill(MIRANDA);
    group.send_back(MIRANDA);
    let mut group2 = group.clone();

    group.kill(JACOB);
    group2 = apply_final_check(group2, THANE, ZAEED);
    println!("{:?}\n\n{:?}", group, group2);
    assert_eq!(group, group2);
}

#[test]
fn check_niche_long_walk_1() {
    let mut group = get_group(0b001001110010, 0);
    group.kill(JACK);
    let group2 = group.clone();
    group.kill(GARRUS);
    println!("{:0>12b}", group.living());
    let nor = Normandy{ armor: true, shields: true, thanix: true, crew: true };
    let (group2, _) = apply_long_walk_check(group2, nor, MIRANDA, MORDIN, GARRUS, JACOB, NONE);
    assert_eq!(group, group2);
}

#[test]
fn solve_all_kas_absent() {
    let mut fails = 0;
    for d in 0..2048_u32 {
        if d.count_ones() < 1 {
            continue;
        }
        let to_die = cl_to_deathgroup(d +  2048, 2047);
        let (solved, _) = main_solve(&to_die, 2047);
        if !solved {
            fails += 1;
            println!("{:0>12b}", d);
        }
    }
    assert_eq!(fails, 0);
}

#[test]
fn solve_all_zaeed_absent() {
    let mut fails = 0;
    for d in 1024..4096_u32 {
        if d.count_ones() < 1 {
            continue;
        }
        if d & 2_u32.pow(ZAEED - 1) != 2_u32.pow(ZAEED - 1) {
            continue;
        }
        let to_die = cl_to_deathgroup(d, 3071);
        let (solved, _) = main_solve(&to_die, 3071);
        if !solved {
            fails += 1;
            println!("{:0>12b}", d);
        }
    }
    assert_eq!(fails, 0);
}

#[test]
fn solve_all_both_dlc_absent() {
    let mut fails = 0;
    for d in 0..1024_u32 {
        if d.count_ones() < 1 {
            continue;
        }
        let to_die = cl_to_deathgroup(d +  3072, 2047);
        let (solved, _) = main_solve(&to_die, 2047);
        if !solved {
            fails += 1;
            println!("{:0>12b}", d);
        }
    }
    assert_eq!(fails, 0);
}

#[test]
#[ignore]
fn solv_exhaustiv_0_1024() {
    let mut fails = 0;
    let mut iters = 0;
    let mut ignored = 0;
    'pres: for p in 0..1024_u32 {
        'kill: for d in 0..4096_u32 {
            for c in 1..13 {
                if !is_present(d, c) && !is_present(p, c) {
                    continue 'kill;
                }
            }
            if !is_present(p, TALI) && !is_present(p, LEGION) && !is_present(p, KASUMI) {
                if is_present(d, GARRUS) && is_present(d, JACOB) && is_present(d, THANE) && is_present(d, MORDIN) {
                    ignored += 1;
                    continue 'kill;
                }
            }
            let to_die = cl_to_deathgroup(d, p);
            if !is_present(p, TALI) && to_die.len() == 0 {
                ignored += 1;
                continue 'kill;
            }
            iters += 1;
            let (solved, _) = main_solve(&to_die, p);
            if !solved {
                fails += 1;
                //println!("{:0>12b}; {:0>12b}", d, p);
            }
        }
    }
    println!("{}/{}; ({} ignored)", fails, iters, ignored);
    assert_eq!(fails, 0);
}

#[test]
#[ignore]
fn solv_exhaustiv_1024_2048() {
    let mut fails = 0;
    let mut iters = 0;
    let mut ignored = 0;
    'pres: for p in 1024..2048_u32 {
        'kill: for d in 0..4096_u32 {
            for c in 1..13 {
                if !is_present(d, c) && !is_present(p, c) {
                    continue 'kill;
                }
            }
            if !is_present(p, TALI) && !is_present(p, LEGION) && !is_present(p, KASUMI) {
                if is_present(d, GARRUS) && is_present(d, JACOB) && is_present(d, THANE) && is_present(d, MORDIN) {
                    ignored += 1;
                    continue 'kill;
                }
            }
            let to_die = cl_to_deathgroup(d, p);
            if !is_present(p, TALI) && to_die.len() == 0 {
                ignored += 1;
                continue 'kill;
            }
            iters += 1;
            let (solved, _) = main_solve(&to_die, p);
            if !solved {
                fails += 1;
                //println!("{:0>12b}; {:0>12b}", d, p);
            }
        }
    }
    println!("{}/{}; ({} ignored)", fails, iters, ignored);
    assert_eq!(fails, 0);
}

#[test]
#[ignore]
fn solv_exhaustiv_2048_3072() {
    let mut fails = 0;
    let mut iters = 0;
    let mut ignored = 0;
    'pres: for p in 2048..3072_u32 {
        'kill: for d in 0..4096_u32 {
            for c in 1..13 {
                if !is_present(d, c) && !is_present(p, c) {
                    continue 'kill;
                }
            }
            if !is_present(p, TALI) && !is_present(p, LEGION) && !is_present(p, KASUMI) {
                if is_present(d, GARRUS) && is_present(d, JACOB) && is_present(d, THANE) && is_present(d, MORDIN) {
                    ignored += 1;
                    continue 'kill;
                }
            }
            let to_die = cl_to_deathgroup(d, p);
            if !is_present(p, TALI) && to_die.len() == 0 {
                ignored += 1;
                continue 'kill;
            }
            iters += 1;
            let (solved, _) = main_solve(&to_die, p);
            if !solved {
                fails += 1;
                //println!("{:0>12b}; {:0>12b}", d, p);
            }
        }
    }
    println!("{}/{}; ({} ignored)", fails, iters, ignored);
    assert_eq!(fails, 0);
}

#[test]
#[ignore]
fn solv_exhaustiv_3072_4096() {
    let mut fails = 0;
    let mut iters = 0;
    let mut ignored = 0;
    'pres: for p in 3072..4096_u32 {
        'kill: for d in 0..4096_u32 {
            for c in 1..13 {
                if !is_present(d, c) && !is_present(p, c) {
                    continue 'kill;
                }
            }
            if !is_present(p, TALI) && !is_present(p, LEGION) && !is_present(p, KASUMI) {
                if is_present(d, GARRUS) && is_present(d, JACOB) && is_present(d, THANE) && is_present(d, MORDIN) {
                    ignored += 1;
                    continue 'kill;
                }
            }
            let to_die = cl_to_deathgroup(d, p);
            if !is_present(p, TALI) && to_die.len() == 0 {
                ignored += 1;
                continue 'kill;
            }
            iters += 1;
            let (solved, _) = main_solve(&to_die, p);
            if !solved {
                fails += 1;
                //println!("{:0>12b}; {:0>12b}", d, p);
            }
        }
    }
    println!("{}/{}; ({} ignored)", fails, iters, ignored);
    assert_eq!(fails, 0);
}

#[test]
#[ignore]
fn solv_two_assured_0_1024() {
    let mut fails = 0;
    let mut iters = 0;
    let mut ignored = 0;
    'pres: for p in 0..1024_u32 {
        if !is_present(p, MIRANDA) || !is_present(p, JACOB) {
            continue 'pres;
        }
        'kill: for d in 0..4096_u32 {
            for c in 1..13 {
                if !is_present(d, c) && !is_present(p, c) {
                    continue 'kill;
                }
            }
            if !is_present(p, TALI) && !is_present(p, LEGION) && !is_present(p, KASUMI) {
                if is_present(d, GARRUS) && is_present(d, JACOB) && is_present(d, THANE) && is_present(d, MORDIN) {
                    ignored += 1;
                    continue 'kill;
                }
            }
            let to_die = cl_to_deathgroup(d, p);
            if !is_present(p, TALI) && to_die.len() == 0 {
                ignored += 1;
                continue 'kill;
            }
            iters += 1;
            let (solved, _) = main_solve(&to_die, p);
            if !solved {
                fails += 1;
                //println!("{:0>12b}; {:0>12b}", d, p);
            }
        }
    }
    println!("{}/{}; ({} ignored)", fails, iters, ignored);
    assert_eq!(fails, 0);
}

#[test]
#[ignore]
fn solv_two_assured_1024_2048() {
    let mut fails = 0;
    let mut iters = 0;
    let mut ignored = 0;
    'pres: for p in 1024..2048_u32 {
        if !is_present(p, MIRANDA) || !is_present(p, JACOB) {
            continue 'pres;
        }
        'kill: for d in 0..4096_u32 {
            for c in 1..13 {
                if !is_present(d, c) && !is_present(p, c) {
                    continue 'kill;
                }
            }
            if !is_present(p, TALI) && !is_present(p, LEGION) && !is_present(p, KASUMI) {
                if is_present(d, GARRUS) && is_present(d, JACOB) && is_present(d, THANE) && is_present(d, MORDIN) {
                    ignored += 1;
                    continue 'kill;
                }
            }
            let to_die = cl_to_deathgroup(d, p);
            if !is_present(p, TALI) && to_die.len() == 0 {
                ignored += 1;
                continue 'kill;
            }
            iters += 1;
            let (solved, _) = main_solve(&to_die, p);
            if !solved {
                fails += 1;
                //println!("{:0>12b}; {:0>12b}", d, p);
            }
        }
    }
    println!("{}/{}; ({} ignored)", fails, iters, ignored);
    assert_eq!(fails, 0);
}

#[test]
#[ignore]
fn solv_two_assured_2048_3072() {
    let mut fails = 0;
    let mut iters = 0;
    let mut ignored = 0;
    'pres: for p in 2048..3072_u32 {
        if !is_present(p, MIRANDA) || !is_present(p, JACOB) {
            continue 'pres;
        }
        'kill: for d in 0..4096_u32 {
            for c in 1..13 {
                if !is_present(d, c) && !is_present(p, c) {
                    continue 'kill;
                }
            }
            if !is_present(p, TALI) && !is_present(p, LEGION) && !is_present(p, KASUMI) {
                if is_present(d, GARRUS) && is_present(d, JACOB) && is_present(d, THANE) && is_present(d, MORDIN) {
                    ignored += 1;
                    continue 'kill;
                }
            }
            let to_die = cl_to_deathgroup(d, p);
            if !is_present(p, TALI) && to_die.len() == 0 {
                ignored += 1;
                continue 'kill;
            }
            iters += 1;
            let (solved, _) = main_solve(&to_die, p);
            if !solved {
                fails += 1;
                //println!("{:0>12b}; {:0>12b}", d, p);
            }
        }
    }
    println!("{}/{}; ({} ignored)", fails, iters, ignored);
    assert_eq!(fails, 0);
}

#[test]
#[ignore]
fn solv_two_assured_3072_4096() {
    let mut fails = 0;
    let mut iters = 0;
    let mut ignored = 0;
    'pres: for p in 3072..4096_u32 {
        if !is_present(p, MIRANDA) || !is_present(p, JACOB) {
            continue 'pres;
        }
        'kill: for d in 0..4096_u32 {
            for c in 1..13 {
                if !is_present(d, c) && !is_present(p, c) {
                    continue 'kill;
                }
            }
            if !is_present(p, TALI) && !is_present(p, LEGION) && !is_present(p, KASUMI) {
                if is_present(d, GARRUS) && is_present(d, JACOB) && is_present(d, THANE) && is_present(d, MORDIN) {
                    ignored += 1;
                    continue 'kill;
                }
            }
            let to_die = cl_to_deathgroup(d, p);
            if !is_present(p, TALI) && to_die.len() == 0 {
                ignored += 1;
                continue 'kill;
            }
            iters += 1;
            let (solved, _) = main_solve(&to_die, p);
            if !solved {
                fails += 1;
                //println!("{:0>12b}; {:0>12b}", d, p);
            }
        }
    }
    println!("{}/{}; ({} ignored)", fails, iters, ignored);
    assert_eq!(fails, 0);
}

#[test]
#[ignore]
fn solv_seven_assured_0_1024() {
    let mut fails = 0;
    let mut iters = 0;
    let mut ignored = 0;
    'pres: for p in 0..1024_u32 {
        if !is_present(p, GARRUS) || !is_present(p, MIRANDA) || !is_present(p, JACOB) || !is_present(p, JACK) || !is_present(p, MORDIN) {
            continue 'pres;
        }
        if ( p.count_ones() < 7 && !is_present(p, GRUNT) && !is_present(p, LEGION) ) || ( p.count_ones() < 8 && (is_present(p, GRUNT) || is_present(p, LEGION)) ) || ( p.count_ones() < 9 && (is_present(p, GRUNT) && is_present(p, LEGION)) ) {
            continue 'pres;
        }
        'kill: for d in 0..4096_u32 {
            for c in 1..13 {
                if !is_present(d, c) && !is_present(p, c) {
                    continue 'kill;
                }
            }
            if !is_present(p, TALI) && !is_present(p, LEGION) && !is_present(p, KASUMI) {
                if is_present(d, GARRUS) && is_present(d, JACOB) && is_present(d, THANE) && is_present(d, MORDIN) {
                    ignored += 1;
                    continue 'kill;
                }
            }
            let to_die = cl_to_deathgroup(d, p);
            if !is_present(p, TALI) && to_die.len() == 0 {
                ignored += 1;
                continue 'kill;
            }
            iters += 1;
            let (solved, _) = main_solve(&to_die, p);
            if !solved {
                fails += 1;
                //println!("{:0>12b}; {:0>12b}", d, p);
            }
        }
    }
    println!("{}/{}; ({} ignored)", fails, iters, ignored);
    assert_eq!(fails, 0);
}

#[test]
#[ignore]
fn solv_seven_assured_1024_2048() {
    let mut fails = 0;
    let mut iters = 0;
    let mut ignored = 0;
    'pres: for p in 1024..2048_u32 {
        if !is_present(p, GARRUS) || !is_present(p, MIRANDA) || !is_present(p, JACOB) || !is_present(p, JACK) || !is_present(p, MORDIN) {
            continue 'pres;
        }
        if ( p.count_ones() < 7 && !is_present(p, GRUNT) && !is_present(p, LEGION) ) || ( p.count_ones() < 8 && (is_present(p, GRUNT) || is_present(p, LEGION)) ) || ( p.count_ones() < 9 && (is_present(p, GRUNT) && is_present(p, LEGION)) ) {
            continue 'pres;
        }
        'kill: for d in 0..4096_u32 {
            for c in 1..13 {
                if !is_present(d, c) && !is_present(p, c) {
                    continue 'kill;
                }
            }
            if !is_present(p, TALI) && !is_present(p, LEGION) && !is_present(p, KASUMI) {
                if is_present(d, GARRUS) && is_present(d, JACOB) && is_present(d, THANE) && is_present(d, MORDIN) {
                    ignored += 1;
                    continue 'kill;
                }
            }
            let to_die = cl_to_deathgroup(d, p);
            if !is_present(p, TALI) && to_die.len() == 0 {
                ignored += 1;
                continue 'kill;
            }
            iters += 1;
            let (solved, _) = main_solve(&to_die, p);
            if !solved {
                fails += 1;
                //println!("{:0>12b}; {:0>12b}", d, p);
            }
        }
    }
    println!("{}/{}; ({} ignored)", fails, iters, ignored);
    assert_eq!(fails, 0);
}

#[test]
#[ignore]
fn solv_seven_assured_2048_3072() {
    let mut fails = 0;
    let mut iters = 0;
    let mut ignored = 0;
    'pres: for p in 2048..3072_u32 {
        if !is_present(p, GARRUS) || !is_present(p, MIRANDA) || !is_present(p, JACOB) || !is_present(p, JACK) || !is_present(p, MORDIN) {
            continue 'pres;
        }
        if ( p.count_ones() < 7 && !is_present(p, GRUNT) && !is_present(p, LEGION) ) || ( p.count_ones() < 8 && (is_present(p, GRUNT) || is_present(p, LEGION)) ) || ( p.count_ones() < 9 && (is_present(p, GRUNT) && is_present(p, LEGION)) ) {
            continue 'pres;
        }
        'kill: for d in 0..4096_u32 {
            for c in 1..13 {
                if !is_present(d, c) && !is_present(p, c) {
                    continue 'kill;
                }
            }
            if !is_present(p, TALI) && !is_present(p, LEGION) && !is_present(p, KASUMI) {
                if is_present(d, GARRUS) && is_present(d, JACOB) && is_present(d, THANE) && is_present(d, MORDIN) {
                    ignored += 1;
                    continue 'kill;
                }
            }
            let to_die = cl_to_deathgroup(d, p);
            if !is_present(p, TALI) && to_die.len() == 0 {
                ignored += 1;
                continue 'kill;
            }
            iters += 1;
            let (solved, _) = main_solve(&to_die, p);
            if !solved {
                fails += 1;
                //println!("{:0>12b}; {:0>12b}", d, p);
            }
        }
    }
    println!("{}/{}; ({} ignored)", fails, iters, ignored);
    assert_eq!(fails, 0);
}

//These run identically to the Five_assured variants, so run those instead.
//#[test]
//#[ignore]
//fn solv_seven_assured_3072_3840() {
//    let mut fails = 0;
//    let mut iters = 0;
//    let mut ignored = 0;
//    'pres: for p in 3072..3840_u32 {
//        if !is_present(p, GARRUS) || !is_present(p, MIRANDA) || !is_present(p, JACOB) || !is_present(p, JACK) || !is_present(p, MORDIN) {
//            continue 'pres;
//        }
//        if ( p.count_ones() < 7 && !is_present(p, GRUNT) && !is_present(p, LEGION) ) || ( p.count_ones() < 8 && (is_present(p, GRUNT) || is_present(p, LEGION)) ) || ( p.count_ones() < 9 && (is_present(p, GRUNT) && is_present(p, LEGION)) ) {
//            continue 'pres;
//        }
//        'kill: for d in 0..4096_u32 {
//            for c in 1..13 {
//                if !is_present(d, c) && !is_present(p, c) {
//                    continue 'kill;
//                }
//            }
//            if !is_present(p, TALI) && !is_present(p, LEGION) && !is_present(p, KASUMI) {
//                if is_present(d, GARRUS) && is_present(d, JACOB) && is_present(d, THANE) && is_present(d, MORDIN) {
//                    ignored += 1;
//                    continue 'kill;
//                }
//            }
//            let to_die = cl_to_deathgroup(d, p);
//            if !is_present(p, TALI) && to_die.len() == 0 {
//                ignored += 1;
//                continue 'kill;
//            }
//            iters += 1;
//            let (solved, _) = main_solve(&to_die, p);
//            if !solved {
//                fails += 1;
//                //println!("{:0>12b}; {:0>12b}", d, p);
//            }
//        }
//    }
//    println!("{}/{}; ({} ignored)", fails, iters, ignored);
//    assert_eq!(fails, 0);
//}

//#[test]
//#[ignore]
//fn solv_seven_assured_3840_4096() {
//    let mut fails = 0;
//    let mut iters = 0;
//    let mut ignored = 0;
//    'pres: for p in 3840..4096_u32 {
//        if !is_present(p, GARRUS) || !is_present(p, MIRANDA) || !is_present(p, JACOB) || !is_present(p, JACK) || !is_present(p, MORDIN) {
//            continue 'pres;
//        }
//        if ( p.count_ones() < 7 && !is_present(p, GRUNT) && !is_present(p, LEGION) ) || ( p.count_ones() < 8 && (is_present(p, GRUNT) || is_present(p, LEGION)) ) || ( p.count_ones() < 9 && (is_present(p, GRUNT) && is_present(p, LEGION)) ) {
//            continue 'pres;
//        }
//        'kill: for d in 0..4096_u32 {
//            for c in 1..13 {
//                if !is_present(d, c) && !is_present(p, c) {
//                    continue 'kill;
//                }
//            }
//            if !is_present(p, TALI) && !is_present(p, LEGION) && !is_present(p, KASUMI) {
//                if is_present(d, GARRUS) && is_present(d, JACOB) && is_present(d, THANE) && is_present(d, MORDIN) {
//                    ignored += 1;
//                    continue 'kill;
//                }
//            }
//            let to_die = cl_to_deathgroup(d, p);
//            if !is_present(p, TALI) && to_die.len() == 0 {
//                ignored += 1;
//                continue 'kill;
//            }
//            iters += 1;
//            let (solved, _) = main_solve(&to_die, p);
//            if !solved {
//                fails += 1;
//                //println!("{:0>12b}; {:0>12b}", d, p);
//            }
//        }
//    }
//    println!("{}/{}; ({} ignored)", fails, iters, ignored);
//    assert_eq!(fails, 0);
//}

#[test]
#[ignore]
fn solv_five_assured_0_1024() {
    let mut fails = 0;
    let mut iters = 0;
    let mut ignored = 0;
    'pres: for p in 0..1024_u32 {
        if !is_present(p, GARRUS) || !is_present(p, MIRANDA) || !is_present(p, JACOB) || !is_present(p, JACK) || !is_present(p, MORDIN) {
            continue 'pres;
        }
        'kill: for d in 0..4096_u32 {
            for c in 1..13 {
                if !is_present(d, c) && !is_present(p, c) {
                    continue 'kill;
                }
            }
            if !is_present(p, TALI) && !is_present(p, LEGION) && !is_present(p, KASUMI) {
                if is_present(d, GARRUS) && is_present(d, JACOB) && is_present(d, THANE) && is_present(d, MORDIN) {
                    ignored += 1;
                    continue 'kill;
                }
            }
            let to_die = cl_to_deathgroup(d, p);
            if !is_present(p, TALI) && to_die.len() == 0 {
                ignored += 1;
                continue 'kill;
            }
            iters += 1;
            let (solved, _) = main_solve(&to_die, p);
            if !solved {
                fails += 1;
                //println!("{:0>12b}; {:0>12b}", d, p);
            }
        }
    }
    println!("{}/{}; ({} ignored)", fails, iters, ignored);
    assert_eq!(fails, 0);
}

#[test]
#[ignore]
fn solv_five_assured_1024_2048() {
    let mut fails = 0;
    let mut iters = 0;
    let mut ignored = 0;
    'pres: for p in 1024..2048_u32 {
        if !is_present(p, GARRUS) || !is_present(p, MIRANDA) || !is_present(p, JACOB) || !is_present(p, JACK) || !is_present(p, MORDIN) {
            continue 'pres;
        }
        'kill: for d in 0..4096_u32 {
            for c in 1..13 {
                if !is_present(d, c) && !is_present(p, c) {
                    continue 'kill;
                }
            }
            if !is_present(p, TALI) && !is_present(p, LEGION) && !is_present(p, KASUMI) {
                if is_present(d, GARRUS) && is_present(d, JACOB) && is_present(d, THANE) && is_present(d, MORDIN) {
                    ignored += 1;
                    continue 'kill;
                }
            }
            let to_die = cl_to_deathgroup(d, p);
            if !is_present(p, TALI) && to_die.len() == 0 {
                ignored += 1;
                continue 'kill;
            }
            iters += 1;
            let (solved, _) = main_solve(&to_die, p);
            if !solved {
                fails += 1;
                //println!("{:0>12b}; {:0>12b}", d, p);
            }
        }
    }
    println!("{}/{}; ({} ignored)", fails, iters, ignored);
    assert_eq!(fails, 0);
}

#[test]
#[ignore]
fn solv_five_assured_2048_3072() {
    let mut fails = 0;
    let mut iters = 0;
    let mut ignored = 0;
    'pres: for p in 2048..3072_u32 {
        if !is_present(p, GARRUS) || !is_present(p, MIRANDA) || !is_present(p, JACOB) || !is_present(p, JACK) || !is_present(p, MORDIN) {
            continue 'pres;
        }
        'kill: for d in 0..4096_u32 {
            for c in 1..13 {
                if !is_present(d, c) && !is_present(p, c) {
                    continue 'kill;
                }
            }
            if !is_present(p, TALI) && !is_present(p, LEGION) && !is_present(p, KASUMI) {
                if is_present(d, GARRUS) && is_present(d, JACOB) && is_present(d, THANE) && is_present(d, MORDIN) {
                    ignored += 1;
                    continue 'kill;
                }
            }
            let to_die = cl_to_deathgroup(d, p);
            if !is_present(p, TALI) && to_die.len() == 0 {
                ignored += 1;
                continue 'kill;
            }
            iters += 1;
            let (solved, _) = main_solve(&to_die, p);
            if !solved {
                fails += 1;
                //println!("{:0>12b}; {:0>12b}", d, p);
            }
        }
    }
    println!("{}/{}; ({} ignored)", fails, iters, ignored);
    assert_eq!(fails, 0);
}

#[test]
#[ignore]
fn solv_five_assured_3072_3840() {
    let mut fails = 0;
    let mut iters = 0;
    let mut ignored = 0;
    'pres: for p in 3072..3840_u32 {
        if !is_present(p, GARRUS) || !is_present(p, MIRANDA) || !is_present(p, JACOB) || !is_present(p, JACK) || !is_present(p, MORDIN) {
            continue 'pres;
        }
        'kill: for d in 0..4096_u32 {
            for c in 1..13 {
                if !is_present(d, c) && !is_present(p, c) {
                    continue 'kill;
                }
            }
            if !is_present(p, TALI) && !is_present(p, LEGION) && !is_present(p, KASUMI) {
                if is_present(d, GARRUS) && is_present(d, JACOB) && is_present(d, THANE) && is_present(d, MORDIN) {
                    ignored += 1;
                    continue 'kill;
                }
            }
            let to_die = cl_to_deathgroup(d, p);
            if !is_present(p, TALI) && to_die.len() == 0 {
                ignored += 1;
                continue 'kill;
            }
            iters += 1;
            let (solved, _) = main_solve(&to_die, p);
            if !solved {
                fails += 1;
                //println!("{:0>12b}; {:0>12b}", d, p);
            }
        }
    }
    println!("{}/{}; ({} ignored)", fails, iters, ignored);
    assert_eq!(fails, 0);
}

#[test]
#[ignore]
fn solv_five_assured_3840_4096() {
    let mut fails = 0;
    let mut iters = 0;
    let mut ignored = 0;
    'pres: for p in 3840..4096_u32 {
        if !is_present(p, GARRUS) || !is_present(p, MIRANDA) || !is_present(p, JACOB) || !is_present(p, JACK) || !is_present(p, MORDIN) {
            continue 'pres;
        }
        'kill: for d in 0..4096_u32 {
            for c in 1..13 {
                if !is_present(d, c) && !is_present(p, c) {
                    continue 'kill;
                }
            }
            if !is_present(p, TALI) && !is_present(p, LEGION) && !is_present(p, KASUMI) {
                if is_present(d, GARRUS) && is_present(d, JACOB) && is_present(d, THANE) && is_present(d, MORDIN) {
                    ignored += 1;
                    continue 'kill;
                }
            }
            let to_die = cl_to_deathgroup(d, p);
            if !is_present(p, TALI) && to_die.len() == 0 {
                ignored += 1;
                continue 'kill;
            }
            iters += 1;
            let (solved, _) = main_solve(&to_die, p);
            if !solved {
                fails += 1;
                //println!("{:0>12b}; {:0>12b}", d, p);
            }
        }
    }
    println!("{}/{}; ({} ignored)", fails, iters, ignored);
    assert_eq!(fails, 0);
}
